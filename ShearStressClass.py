#!/usr/bin/env python
# coding: utf-8

# # Class Defining Shear Stress Source Term

# In[1]:


import numpy as np


# In[2]:


class ShearStressModel:
    """Class defining a shear stress model"""
    
    def __init__(self, grid, U, rho, mu):
        """Constructor"""
        self._grid = grid
        self._U = U
        self._rho = rho
        self._mu = mu
        
    def add(self, coeffs):
        """Function to add surface convection terms to coefficient arrays"""
        
        # Calculate Re
        Re = self._rho*0.02*self._U[1:-1]/self._mu #0.02 is the hydraulic diameter of the duct, D = 4A/P
        
        # Calculate the shear stress 'source term'
        source = ((1.58*np.log(Re)-3.28)**-2)*0.5*self._rho*self._U[1:-1]**2*self._grid.Ao
        
        # Calculate the linearization coefficients
        coeffP = ((1.58*np.log(Re)-3.28)**-2)*self._rho*self._U[1:-1]*self._grid.Ao
        
        # Add to coefficient arrays
        coeffs.accumulate_aP(coeffP)
        coeffs.accumulate_rP(source)
        
        return coeffs

