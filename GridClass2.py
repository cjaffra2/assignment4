#!/usr/bin/env python
# coding: utf-8

# # Grid class for circular duct of variable cross section

# In[1]:


import numpy as np
import math


# In[2]:


#Grid class for circular duct of variable cross section

import numpy as np
import math

class Grid:
    """Class defining a one-dimensional Cartesian grid"""
    
    def __init__(self, lx, ncv):
        """Constructor
            lx .... total length of domain in x-direction [m]
            ncv ... number of control volumes in domain
        """
        # Store the number of control volumes
        self._ncv = ncv
        
        # Calculate the control volume length
        dx = lx/float(ncv)
        
        # Calculate the face locations
        self._xf = np.array([i*dx for i in range(ncv+1)])
        
        self._radius=2*0.01 + 0.01*np.cos(2*math.pi*self._xf) #Equation for radius as a function of x
        
        # Calculate the cell centroid locations
        self._xP = np.array([self._xf[0]] + 
                            [0.5*(self._xf[i]+self._xf[i+1]) for i in range(ncv)] +
                            [self._xf[-1]])
        self._radiusc=2*0.01 + 0.01*np.cos(2*math.pi*self._xP) #Equation for radius as a function of x
        
        # Calculate face areas
        self._Af = math.pi*self._radius**2
        self._Ac = math.pi*self._radiusc**2
        
        # Calculate the outer surface area for each cell
        self._Ao = dx*2*math.pi*self._radiusc
        
        # Calculate cell volumes
        self._vol = dx*self._Ac[1:-1]
        
    @property
    def ncv(self):
        """Number of control volumes in domain"""
        return self._ncv
    
    @property
    def xf(self):
        """Face location array"""
        return self._xf
    
    @property
    def xP(self):
        """Cell centroid array"""
        return self._xP
    
    @property
    def dx_WP(self):
        return self.xP[1:-1]-self.xP[0:-2]
        
    @property
    def dx_PE(self):
        return self.xP[2:]-self.xP[1:-1]
      
    @property
    def Af(self):
        """Face area array"""
        return self._Af

    @property
    def Aw(self):
        """West face area array"""
        return self._Af[0:-1]
    
    @property
    def Ae(self):
        """East face area array"""
        return self._Af[1:]
    
    @property
    def Ao(self):
        """Outer face area array"""
        return self._Ao
    
    @property
    def vol(self):
        """Cell volume array"""
        return self._vol

