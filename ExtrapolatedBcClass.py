#!/usr/bin/env python
# coding: utf-8

# # Class Defining an Extrapolated Boundary Condition

# In[1]:


import numpy as np
from BoundaryClass import *


# In[2]:


class ExtrapolatedBc:
    """Class defining an extrapolated boundary condition"""

    def __init__(self, phi, grid, loc):
        """Constructor
            phi ........ field variable array
            grid ....... grid
            loc ........ boundary location
        """
        self._phi = phi
        self._grid = grid
        self._loc = loc

    def value(self):
        """Return the boundary condition value"""
        if self._loc is BoundaryLocation.WEST:
            return self._phi[1] - ((self._phi[2] - self._phi[1])/2)
        elif self._loc is BoundaryLocation.EAST:
            return self._phi[-2] + ((self._phi[-2] - self._phi[-3])/2)
        else:
            raise ValueError("Unknown boundary location")

    def coeff(self):
        """Return the linearization coefficient"""
        if self._loc is BoundaryLocation.WEST:
            return 1
        elif self._loc is BoundaryLocation.EAST:
            return 1
        else:
            raise ValueError("Unknown boundary location")

    def apply(self):
        """Applies the boundary condition in the referenced field variable array"""
        if self._loc is BoundaryLocation.WEST:
            pass 
            self._phi[0] = self._phi[1] - ((self._phi[2] - self._phi[1])/2)
        elif self._loc is BoundaryLocation.EAST:
            pass 
            self._phi[-1] = self._phi[-2] + ((self._phi[-2] - self._phi[-3])/2)
        else:
            raise ValueError("Unknown boundary location")

