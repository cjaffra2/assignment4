#!/usr/bin/env python
# coding: utf-8

# # Class Defining Quick Advection Model A4

# In[1]:


import numpy as np


# In[2]:


class QuickAdvectionModel:
    """Class defining a Quick advection model"""

    def __init__(self, grid, phi, Uhe, rho, const, west_bc, east_bc):
        """Constructor"""
        self._grid = grid
        self._phi = phi
        self._Uhe = Uhe
        self._rho = rho
        self._const = const
        self._west_bc = west_bc
        self._east_bc = east_bc
        self._alphae = np.zeros(self._grid.ncv+1)
        self._phie = np.zeros(self._grid.ncv+1)
       
    def add(self, coeffs):
        """Function to add diffusion terms to coefficient arrays"""

        # Calculate the weighting factors
        for i in range(self._grid.ncv+1):
            if self._Uhe[i] >= 0:
                self._alphae[i] = 1
            else:
                self._alphae[i] = -1
        self._alphae[0]=1        
        self._alphae[-1]=1
        
        # Calculate the east integration point values (including both boundaries)
        if self._Uhe[i] >= 0:
            for i in range(1,self._grid.ncv+1):
                xW=self._grid.xP[i-1]
                xP=self._grid.xP[i]
                xE=self._grid.xP[i+1]
                xe=self._grid.xf[i]
            
                self._phie[i] = (((xe-xW)*(xe-xE))/((xP-xW)*(xP-xE)))*self._phi[i] + (((xe-xW)*(xe-xP))/((xE-xW)*(xE-xP)))*self._phi[i+1] + (((xe-xP)*(xe-xE))/((xW-xP)*(xW-xE)))*self._phi[i-1]
    
        else:
            for i in range(1,self._grid.ncv-1):
                xP=self._grid.xP[i]
                xE=self._grid.xP[i+1]
                xEE=self._grid.xP[i+2]
                xe=self._grid.xf[i]
            
                self._phie[i] = (((xe-xEE)*(xe-xP))/((xE-xEE)*(xE-xP)))*self._phi[i] + (((xe-xEE)*(xe-xE))/((xP-xEE)*(xP-xE)))*self._phi[i+1] + (((xe-xE)*(xe-xP))/((xEE-xE)*(xEE-xP)))*self._phi[i+2]
            
        
        self._phie[0] = (1 + self._alphae[0])/2*self._phi[0] + (1 - self._alphae[0])/2*self._phi[1]
        self._phie[-1] = (1 + self._alphae[-1])/2*self._phi[-2] + (1 - self._alphae[-1])/2*self._phi[-1]
        
        # Calculate the face mass fluxes
        mdote = self._rho*self._Uhe*self._grid.Af
        
        # Calculate the west and east face advection flux terms for each face
        flux_w = self._const*mdote[:-1]*self._phie[:-1]
        flux_e = self._const*mdote[1:]*self._phie[1:]
        
        # Calculate mass imbalance term
        imbalance = - self._const*mdote[1:]*self._phi[1:-1] + self._const*mdote[:-1]*self._phi[1:-1]
          
        # Calculate the linearization coefficients
        coeffW = - self._const*mdote[:-1]*(1 + self._alphae[:-1])/2
        coeffE = self._const*mdote[1:]*(1 - self._alphae[1:])/2
        coeffP = - coeffW - coeffE

        # Modify the linearization coefficients on the boundaries
        coeffP[0] += coeffW[0]*self._west_bc.coeff()
        coeffP[-1] += coeffE[-1]*self._east_bc.coeff()

        # Zero the boundary coefficients that are not used
        coeffW[0] = 0.0
        coeffE[-1] = 0.0

        # Calculate the net flux from each cell
        flux = flux_e - flux_w

        # Add to coefficient arrays
        coeffs.accumulate_aP(coeffP)
        coeffs.accumulate_aW(coeffW)
        coeffs.accumulate_aE(coeffE)
        coeffs.accumulate_rP(flux)
        coeffs.accumulate_rP(imbalance)

        # Return the modified coefficient array
        return coeffs

