#!/usr/bin/env python
# coding: utf-8

# ## Functions for Different Advection Models (FunctionVar = NCV)

# In[1]:


from GridClass2 import *
from CoefficientsClass import *
from DiffusionClass import *
from ConvectionClass import *
from LinearSolver import *
from DirichletClass import *
from NeumannClass import *
from BoundaryClass import *
from FirstOrderTransientModelClass2 import *
from UDSClass2 import *
from CDSClass2 import *
from QuickClass2 import *
from AdvectingVelocityClass import *
from PressureForceClass import *
from MassConservationClass import *
from ExtrapolatedBcClass import *

import numpy as np
from numpy.linalg import norm


# In[2]:


# This function returns Velocity Solution for UDS Advection Model

def UDSFunctionU(nNCV):

    lx = 1.0
    ncv = nNCV
    grid = Grid(lx, ncv)

    # Set the timestep information
    nTime = 1
    dt = 1e9
    time = 0

    # Set the maximum number of iterations and convergence criterion
    maxIter = 100
    converged = 1e-6

    # Define thermophysical properties
    rho = 1000
    mu = 1e-3

    # Define the coefficients
    PU_coeffs = ScalarCoeffs(grid.ncv)
    PP_coeffs = ScalarCoeffs(grid.ncv)
    UP_coeffs = ScalarCoeffs(grid.ncv)
    UU_coeffs = ScalarCoeffs(grid.ncv)

    # Initial conditions
    U0 = 2
    P0 = 0

    # Initialize field variable arrays
    U = U0*np.ones(grid.ncv+2)
    P = P0*np.ones(grid.ncv+2)

    # Initialize advecting velocity and damping coefficient array
    dhat = np.zeros(grid.ncv+1)
    Uhe = U0*np.ones(grid.ncv+1)

    # Define boundary conditions for velocity
    U_west_bc = DirichletBc(U, grid, U0, BoundaryLocation.WEST)
    U_east_bc = NeumannBc(U, grid, 0, BoundaryLocation.EAST)

    # Define boundary conditions for pressure
    #   - Once ExtrapolatedBc is complete, change the boundary condition
    P_west_bc = ExtrapolatedBc(P, grid, BoundaryLocation.WEST)
    #P_west_bc = NeumannBc(P, grid, 0, BoundaryLocation.WEST)
    P_east_bc = DirichletBc(P, grid, 0, BoundaryLocation.EAST)

    # Apply boundary conditions
    U_west_bc.apply()
    U_east_bc.apply()
    P_west_bc.apply()
    P_east_bc.apply()

    # Create list to store the solutions at each iteration
    U_solns = [np.copy(U)]
    P_solns = [np.copy(P)]

    # Define the transient model
    Uold = np.copy(U)
    transient = FirstOrderTransientModel(grid, U, Uold, rho, 1, dt)

    # Define the diffusion model
    diffusion = DiffusionModel(grid, U, mu, U_west_bc, U_east_bc)

    # Define the advection model
    advection = UpwindAdvectionModel(grid, U, Uhe, rho, 1, U_west_bc, U_east_bc)

    # Define the pressure force model
    pressure = PressureForceModel(grid, P, P_west_bc, P_east_bc)

    # Define advecting velocity model
    advecting = AdvectingVelocityModel(grid, dhat, Uhe, P, U, UU_coeffs)

    # Define conservation of mass equation
    mass = MassConservationEquation(grid, U, P, dhat, Uhe, rho, 
                                    P_west_bc, P_east_bc, U_west_bc, U_east_bc)

    # Loop through all timesteps
    for tStep in range(nTime):
        # Update the time information
        time += dt

        # Print the timestep information
        print("Timestep = {}; Time = {}".format(tStep, time))

        # Store the "old" velocity field
        Uold[:] = U[:]

        # Iterate until the solution is converged
        for i in range(maxIter):

            # Zero all of the equations
            PP_coeffs.zero()
            PU_coeffs.zero()
            UU_coeffs.zero()
            UP_coeffs.zero()     

            # Assemble the momentum equations
            #   Note: do this before mass, because the coeffs are needed to compute advecting velocity
            UU_coeffs = diffusion.add(UU_coeffs)
            UU_coeffs = advection.add(UU_coeffs)
            UU_coeffs = transient.add(UU_coeffs)
            UP_coeffs = pressure.add(UP_coeffs)

            # Assemble the mass equations
            advecting.update()
            PP_coeffs, PU_coeffs = mass.add(PP_coeffs, PU_coeffs)

            # Compute residuals and check for convergence
            PmaxResid = norm(PU_coeffs.rP + PP_coeffs.rP, np.inf)
            PavgResid = np.mean(np.absolute(PU_coeffs.rP + PP_coeffs.rP))
            UmaxResid = norm(UU_coeffs.rP + UP_coeffs.rP, np.inf)
            UavgResid = np.mean(np.absolute(UU_coeffs.rP + UP_coeffs.rP))
            print("Iteration = {}.".format(i))
            print("  Mass:     Max. Resid. = {}; Avg. Resid. = {}".format(PmaxResid, PavgResid))
            print("  Momentum: Max. Resid. = {}; Avg. Resid. = {}".format(UmaxResid, UavgResid))
            if PmaxResid < converged and UmaxResid < converged:
                break

            # Solve the sparse matrix system
            dP, dU = solve(PP_coeffs, PU_coeffs, UP_coeffs, UU_coeffs)

            # Update the solutions 
            P[1:-1] += dP
            U[1:-1] += dU

            # Update boundary conditions
            U_west_bc.apply()
            U_east_bc.apply()
            P_west_bc.apply()
            P_east_bc.apply()

            # Update the advecting velocities
            advecting.update()

            # Store the solution
            P_solns.append(np.copy(P))
            U_solns.append(np.copy(U))
        
    return U_solns


# In[3]:


# This function returns Pressure Solution for UDS Advection Model

def UDSFunctionP(nNCV):

    lx = 1.0
    ncv = nNCV
    grid = Grid(lx, ncv)

    # Set the timestep information
    nTime = 1
    dt = 1e9
    time = 0

    # Set the maximum number of iterations and convergence criterion
    maxIter = 100
    converged = 1e-6

    # Define thermophysical properties
    rho = 1000
    mu = 1e-3

    # Define the coefficients
    PU_coeffs = ScalarCoeffs(grid.ncv)
    PP_coeffs = ScalarCoeffs(grid.ncv)
    UP_coeffs = ScalarCoeffs(grid.ncv)
    UU_coeffs = ScalarCoeffs(grid.ncv)

    # Initial conditions
    U0 = 2
    P0 = 0

    # Initialize field variable arrays
    U = U0*np.ones(grid.ncv+2)
    P = P0*np.ones(grid.ncv+2)

    # Initialize advecting velocity and damping coefficient array
    dhat = np.zeros(grid.ncv+1)
    Uhe = U0*np.ones(grid.ncv+1)

    # Define boundary conditions for velocity
    U_west_bc = DirichletBc(U, grid, U0, BoundaryLocation.WEST)
    U_east_bc = NeumannBc(U, grid, 0, BoundaryLocation.EAST)

    # Define boundary conditions for pressure
    #   - Once ExtrapolatedBc is complete, change the boundary condition
    P_west_bc = ExtrapolatedBc(P, grid, BoundaryLocation.WEST)
    #P_west_bc = NeumannBc(P, grid, 0, BoundaryLocation.WEST)
    P_east_bc = DirichletBc(P, grid, 0, BoundaryLocation.EAST)

    # Apply boundary conditions
    U_west_bc.apply()
    U_east_bc.apply()
    P_west_bc.apply()
    P_east_bc.apply()

    # Create list to store the solutions at each iteration
    U_solns = [np.copy(U)]
    P_solns = [np.copy(P)]

    # Define the transient model
    Uold = np.copy(U)
    transient = FirstOrderTransientModel(grid, U, Uold, rho, 1, dt)

    # Define the diffusion model
    diffusion = DiffusionModel(grid, U, mu, U_west_bc, U_east_bc)

    # Define the advection model
    advection = UpwindAdvectionModel(grid, U, Uhe, rho, 1, U_west_bc, U_east_bc)

    # Define the pressure force model
    pressure = PressureForceModel(grid, P, P_west_bc, P_east_bc)

    # Define advecting velocity model
    advecting = AdvectingVelocityModel(grid, dhat, Uhe, P, U, UU_coeffs)

    # Define conservation of mass equation
    mass = MassConservationEquation(grid, U, P, dhat, Uhe, rho, 
                                    P_west_bc, P_east_bc, U_west_bc, U_east_bc)

    # Loop through all timesteps
    for tStep in range(nTime):
        # Update the time information
        time += dt

        # Print the timestep information
        print("Timestep = {}; Time = {}".format(tStep, time))

        # Store the "old" velocity field
        Uold[:] = U[:]

        # Iterate until the solution is converged
        for i in range(maxIter):

            # Zero all of the equations
            PP_coeffs.zero()
            PU_coeffs.zero()
            UU_coeffs.zero()
            UP_coeffs.zero()     

            # Assemble the momentum equations
            #   Note: do this before mass, because the coeffs are needed to compute advecting velocity
            UU_coeffs = diffusion.add(UU_coeffs)
            UU_coeffs = advection.add(UU_coeffs)
            UU_coeffs = transient.add(UU_coeffs)
            UP_coeffs = pressure.add(UP_coeffs)

            # Assemble the mass equations
            advecting.update()
            PP_coeffs, PU_coeffs = mass.add(PP_coeffs, PU_coeffs)

            # Compute residuals and check for convergence
            PmaxResid = norm(PU_coeffs.rP + PP_coeffs.rP, np.inf)
            PavgResid = np.mean(np.absolute(PU_coeffs.rP + PP_coeffs.rP))
            UmaxResid = norm(UU_coeffs.rP + UP_coeffs.rP, np.inf)
            UavgResid = np.mean(np.absolute(UU_coeffs.rP + UP_coeffs.rP))
            print("Iteration = {}.".format(i))
            print("  Mass:     Max. Resid. = {}; Avg. Resid. = {}".format(PmaxResid, PavgResid))
            print("  Momentum: Max. Resid. = {}; Avg. Resid. = {}".format(UmaxResid, UavgResid))
            if PmaxResid < converged and UmaxResid < converged:
                break

            # Solve the sparse matrix system
            dP, dU = solve(PP_coeffs, PU_coeffs, UP_coeffs, UU_coeffs)

            # Update the solutions 
            P[1:-1] += dP
            U[1:-1] += dU

            # Update boundary conditions
            U_west_bc.apply()
            U_east_bc.apply()
            P_west_bc.apply()
            P_east_bc.apply()

            # Update the advecting velocities
            advecting.update()

            # Store the solution
            P_solns.append(np.copy(P))
            U_solns.append(np.copy(U))
        
    return P_solns


# In[4]:


# This function returns Velocity Solution for CDS Advection Model

def CDSFunctionU(nNCV):

    lx = 1.0
    ncv = nNCV
    grid = Grid(lx, ncv)

    # Set the timestep information
    nTime = 1
    dt = 1e9
    time = 0

    # Set the maximum number of iterations and convergence criterion
    maxIter = 100
    converged = 1e-6

    # Define thermophysical properties
    rho = 1000
    mu = 1e-3

    # Define the coefficients
    PU_coeffs = ScalarCoeffs(grid.ncv)
    PP_coeffs = ScalarCoeffs(grid.ncv)
    UP_coeffs = ScalarCoeffs(grid.ncv)
    UU_coeffs = ScalarCoeffs(grid.ncv)

    # Initial conditions
    U0 = 2
    P0 = 0

    # Initialize field variable arrays
    U = U0*np.ones(grid.ncv+2)
    P = P0*np.ones(grid.ncv+2)

    # Initialize advecting velocity and damping coefficient array
    dhat = np.zeros(grid.ncv+1)
    Uhe = U0*np.ones(grid.ncv+1)

    # Define boundary conditions for velocity
    U_west_bc = DirichletBc(U, grid, U0, BoundaryLocation.WEST)
    U_east_bc = NeumannBc(U, grid, 0, BoundaryLocation.EAST)

    # Define boundary conditions for pressure
    #   - Once ExtrapolatedBc is complete, change the boundary condition
    P_west_bc = ExtrapolatedBc(P, grid, BoundaryLocation.WEST)
    #P_west_bc = NeumannBc(P, grid, 0, BoundaryLocation.WEST)
    P_east_bc = DirichletBc(P, grid, 0, BoundaryLocation.EAST)

    # Apply boundary conditions
    U_west_bc.apply()
    U_east_bc.apply()
    P_west_bc.apply()
    P_east_bc.apply()

    # Create list to store the solutions at each iteration
    U_solns = [np.copy(U)]
    P_solns = [np.copy(P)]

    # Define the transient model
    Uold = np.copy(U)
    transient = FirstOrderTransientModel(grid, U, Uold, rho, 1, dt)

    # Define the diffusion model
    diffusion = DiffusionModel(grid, U, mu, U_west_bc, U_east_bc)

    # Define the advection model
    advection = CDSAdvectionModel(grid, U, Uhe, rho, 1, U_west_bc, U_east_bc)

    # Define the pressure force model
    pressure = PressureForceModel(grid, P, P_west_bc, P_east_bc)

    # Define advecting velocity model
    advecting = AdvectingVelocityModel(grid, dhat, Uhe, P, U, UU_coeffs)

    # Define conservation of mass equation
    mass = MassConservationEquation(grid, U, P, dhat, Uhe, rho, 
                                    P_west_bc, P_east_bc, U_west_bc, U_east_bc)

    # Loop through all timesteps
    for tStep in range(nTime):
        # Update the time information
        time += dt

        # Print the timestep information
        print("Timestep = {}; Time = {}".format(tStep, time))

        # Store the "old" velocity field
        Uold[:] = U[:]

        # Iterate until the solution is converged
        for i in range(maxIter):

            # Zero all of the equations
            PP_coeffs.zero()
            PU_coeffs.zero()
            UU_coeffs.zero()
            UP_coeffs.zero()     

            # Assemble the momentum equations
            #   Note: do this before mass, because the coeffs are needed to compute advecting velocity
            UU_coeffs = diffusion.add(UU_coeffs)
            UU_coeffs = advection.add(UU_coeffs)
            UU_coeffs = transient.add(UU_coeffs)
            UP_coeffs = pressure.add(UP_coeffs)

            # Assemble the mass equations
            advecting.update()
            PP_coeffs, PU_coeffs = mass.add(PP_coeffs, PU_coeffs)

            # Compute residuals and check for convergence
            PmaxResid = norm(PU_coeffs.rP + PP_coeffs.rP, np.inf)
            PavgResid = np.mean(np.absolute(PU_coeffs.rP + PP_coeffs.rP))
            UmaxResid = norm(UU_coeffs.rP + UP_coeffs.rP, np.inf)
            UavgResid = np.mean(np.absolute(UU_coeffs.rP + UP_coeffs.rP))
            print("Iteration = {}.".format(i))
            print("  Mass:     Max. Resid. = {}; Avg. Resid. = {}".format(PmaxResid, PavgResid))
            print("  Momentum: Max. Resid. = {}; Avg. Resid. = {}".format(UmaxResid, UavgResid))
            if PmaxResid < converged and UmaxResid < converged:
                break

            # Solve the sparse matrix system
            dP, dU = solve(PP_coeffs, PU_coeffs, UP_coeffs, UU_coeffs)

            # Update the solutions 
            P[1:-1] += dP
            U[1:-1] += dU

            # Update boundary conditions
            U_west_bc.apply()
            U_east_bc.apply()
            P_west_bc.apply()
            P_east_bc.apply()

            # Update the advecting velocities
            advecting.update()

            # Store the solution
            P_solns.append(np.copy(P))
            U_solns.append(np.copy(U))
        
    return U_solns


# In[5]:


# This function returns Pressure Solution for CDS Advection Model

def CDSFunctionP(nNCV):

    lx = 1.0
    ncv = nNCV
    grid = Grid(lx, ncv)

    # Set the timestep information
    nTime = 1
    dt = 1e9
    time = 0

    # Set the maximum number of iterations and convergence criterion
    maxIter = 100
    converged = 1e-6

    # Define thermophysical properties
    rho = 1000
    mu = 1e-3

    # Define the coefficients
    PU_coeffs = ScalarCoeffs(grid.ncv)
    PP_coeffs = ScalarCoeffs(grid.ncv)
    UP_coeffs = ScalarCoeffs(grid.ncv)
    UU_coeffs = ScalarCoeffs(grid.ncv)

    # Initial conditions
    U0 = 2
    P0 = 0

    # Initialize field variable arrays
    U = U0*np.ones(grid.ncv+2)
    P = P0*np.ones(grid.ncv+2)

    # Initialize advecting velocity and damping coefficient array
    dhat = np.zeros(grid.ncv+1)
    Uhe = U0*np.ones(grid.ncv+1)

    # Define boundary conditions for velocity
    U_west_bc = DirichletBc(U, grid, U0, BoundaryLocation.WEST)
    U_east_bc = NeumannBc(U, grid, 0, BoundaryLocation.EAST)

    # Define boundary conditions for pressure
    #   - Once ExtrapolatedBc is complete, change the boundary condition
    P_west_bc = ExtrapolatedBc(P, grid, BoundaryLocation.WEST)
    #P_west_bc = NeumannBc(P, grid, 0, BoundaryLocation.WEST)
    P_east_bc = DirichletBc(P, grid, 0, BoundaryLocation.EAST)

    # Apply boundary conditions
    U_west_bc.apply()
    U_east_bc.apply()
    P_west_bc.apply()
    P_east_bc.apply()

    # Create list to store the solutions at each iteration
    U_solns = [np.copy(U)]
    P_solns = [np.copy(P)]

    # Define the transient model
    Uold = np.copy(U)
    transient = FirstOrderTransientModel(grid, U, Uold, rho, 1, dt)

    # Define the diffusion model
    diffusion = DiffusionModel(grid, U, mu, U_west_bc, U_east_bc)

    # Define the advection model
    advection = CDSAdvectionModel(grid, U, Uhe, rho, 1, U_west_bc, U_east_bc)

    # Define the pressure force model
    pressure = PressureForceModel(grid, P, P_west_bc, P_east_bc)

    # Define advecting velocity model
    advecting = AdvectingVelocityModel(grid, dhat, Uhe, P, U, UU_coeffs)

    # Define conservation of mass equation
    mass = MassConservationEquation(grid, U, P, dhat, Uhe, rho, 
                                    P_west_bc, P_east_bc, U_west_bc, U_east_bc)

    # Loop through all timesteps
    for tStep in range(nTime):
        # Update the time information
        time += dt

        # Print the timestep information
        print("Timestep = {}; Time = {}".format(tStep, time))

        # Store the "old" velocity field
        Uold[:] = U[:]

        # Iterate until the solution is converged
        for i in range(maxIter):

            # Zero all of the equations
            PP_coeffs.zero()
            PU_coeffs.zero()
            UU_coeffs.zero()
            UP_coeffs.zero()     

            # Assemble the momentum equations
            #   Note: do this before mass, because the coeffs are needed to compute advecting velocity
            UU_coeffs = diffusion.add(UU_coeffs)
            UU_coeffs = advection.add(UU_coeffs)
            UU_coeffs = transient.add(UU_coeffs)
            UP_coeffs = pressure.add(UP_coeffs)

            # Assemble the mass equations
            advecting.update()
            PP_coeffs, PU_coeffs = mass.add(PP_coeffs, PU_coeffs)

            # Compute residuals and check for convergence
            PmaxResid = norm(PU_coeffs.rP + PP_coeffs.rP, np.inf)
            PavgResid = np.mean(np.absolute(PU_coeffs.rP + PP_coeffs.rP))
            UmaxResid = norm(UU_coeffs.rP + UP_coeffs.rP, np.inf)
            UavgResid = np.mean(np.absolute(UU_coeffs.rP + UP_coeffs.rP))
            print("Iteration = {}.".format(i))
            print("  Mass:     Max. Resid. = {}; Avg. Resid. = {}".format(PmaxResid, PavgResid))
            print("  Momentum: Max. Resid. = {}; Avg. Resid. = {}".format(UmaxResid, UavgResid))
            if PmaxResid < converged and UmaxResid < converged:
                break

            # Solve the sparse matrix system
            dP, dU = solve(PP_coeffs, PU_coeffs, UP_coeffs, UU_coeffs)

            # Update the solutions 
            P[1:-1] += dP
            U[1:-1] += dU

            # Update boundary conditions
            U_west_bc.apply()
            U_east_bc.apply()
            P_west_bc.apply()
            P_east_bc.apply()

            # Update the advecting velocities
            advecting.update()

            # Store the solution
            P_solns.append(np.copy(P))
            U_solns.append(np.copy(U))
        
    return P_solns


# In[6]:


# This function returns Velocity Solution for Quick Advection Model

def QuickFunctionU(nNCV):

    lx = 1.0
    ncv = nNCV
    grid = Grid(lx, ncv)

    # Set the timestep information
    nTime = 1
    dt = 1e9
    time = 0

    # Set the maximum number of iterations and convergence criterion
    maxIter = 100
    converged = 1e-6

    # Define thermophysical properties
    rho = 1000
    mu = 1e-3

    # Define the coefficients
    PU_coeffs = ScalarCoeffs(grid.ncv)
    PP_coeffs = ScalarCoeffs(grid.ncv)
    UP_coeffs = ScalarCoeffs(grid.ncv)
    UU_coeffs = ScalarCoeffs(grid.ncv)

    # Initial conditions
    U0 = 2
    P0 = 0

    # Initialize field variable arrays
    U = U0*np.ones(grid.ncv+2)
    P = P0*np.ones(grid.ncv+2)

    # Initialize advecting velocity and damping coefficient array
    dhat = np.zeros(grid.ncv+1)
    Uhe = U0*np.ones(grid.ncv+1)

    # Define boundary conditions for velocity
    U_west_bc = DirichletBc(U, grid, U0, BoundaryLocation.WEST)
    U_east_bc = NeumannBc(U, grid, 0, BoundaryLocation.EAST)

    # Define boundary conditions for pressure
    #   - Once ExtrapolatedBc is complete, change the boundary condition
    P_west_bc = ExtrapolatedBc(P, grid, BoundaryLocation.WEST)
    #P_west_bc = NeumannBc(P, grid, 0, BoundaryLocation.WEST)
    P_east_bc = DirichletBc(P, grid, 0, BoundaryLocation.EAST)

    # Apply boundary conditions
    U_west_bc.apply()
    U_east_bc.apply()
    P_west_bc.apply()
    P_east_bc.apply()

    # Create list to store the solutions at each iteration
    U_solns = [np.copy(U)]
    P_solns = [np.copy(P)]

    # Define the transient model
    Uold = np.copy(U)
    transient = FirstOrderTransientModel(grid, U, Uold, rho, 1, dt)

    # Define the diffusion model
    diffusion = DiffusionModel(grid, U, mu, U_west_bc, U_east_bc)

    # Define the advection model
    advection = QuickAdvectionModel(grid, U, Uhe, rho, 1, U_west_bc, U_east_bc)

    # Define the pressure force model
    pressure = PressureForceModel(grid, P, P_west_bc, P_east_bc)

    # Define advecting velocity model
    advecting = AdvectingVelocityModel(grid, dhat, Uhe, P, U, UU_coeffs)

    # Define conservation of mass equation
    mass = MassConservationEquation(grid, U, P, dhat, Uhe, rho, 
                                    P_west_bc, P_east_bc, U_west_bc, U_east_bc)

    # Loop through all timesteps
    for tStep in range(nTime):
        # Update the time information
        time += dt

        # Print the timestep information
        print("Timestep = {}; Time = {}".format(tStep, time))

        # Store the "old" velocity field
        Uold[:] = U[:]

        # Iterate until the solution is converged
        for i in range(maxIter):

            # Zero all of the equations
            PP_coeffs.zero()
            PU_coeffs.zero()
            UU_coeffs.zero()
            UP_coeffs.zero()     

            # Assemble the momentum equations
            #   Note: do this before mass, because the coeffs are needed to compute advecting velocity
            UU_coeffs = diffusion.add(UU_coeffs)
            UU_coeffs = advection.add(UU_coeffs)
            UU_coeffs = transient.add(UU_coeffs)
            UP_coeffs = pressure.add(UP_coeffs)

            # Assemble the mass equations
            advecting.update()
            PP_coeffs, PU_coeffs = mass.add(PP_coeffs, PU_coeffs)

            # Compute residuals and check for convergence
            PmaxResid = norm(PU_coeffs.rP + PP_coeffs.rP, np.inf)
            PavgResid = np.mean(np.absolute(PU_coeffs.rP + PP_coeffs.rP))
            UmaxResid = norm(UU_coeffs.rP + UP_coeffs.rP, np.inf)
            UavgResid = np.mean(np.absolute(UU_coeffs.rP + UP_coeffs.rP))
            print("Iteration = {}.".format(i))
            print("  Mass:     Max. Resid. = {}; Avg. Resid. = {}".format(PmaxResid, PavgResid))
            print("  Momentum: Max. Resid. = {}; Avg. Resid. = {}".format(UmaxResid, UavgResid))
            if PmaxResid < converged and UmaxResid < converged:
                break

            # Solve the sparse matrix system
            dP, dU = solve(PP_coeffs, PU_coeffs, UP_coeffs, UU_coeffs)

            # Update the solutions 
            P[1:-1] += dP
            U[1:-1] += dU

            # Update boundary conditions
            U_west_bc.apply()
            U_east_bc.apply()
            P_west_bc.apply()
            P_east_bc.apply()

            # Update the advecting velocities
            advecting.update()

            # Store the solution
            P_solns.append(np.copy(P))
            U_solns.append(np.copy(U))
        
    return U_solns


# In[7]:


# This function returns Pressure Solution for Quick Advection Model

def QuickFunctionP(nNCV):

    lx = 1.0
    ncv = nNCV
    grid = Grid(lx, ncv)

    # Set the timestep information
    nTime = 1
    dt = 1e9
    time = 0

    # Set the maximum number of iterations and convergence criterion
    maxIter = 100
    converged = 1e-6

    # Define thermophysical properties
    rho = 1000
    mu = 1e-3

    # Define the coefficients
    PU_coeffs = ScalarCoeffs(grid.ncv)
    PP_coeffs = ScalarCoeffs(grid.ncv)
    UP_coeffs = ScalarCoeffs(grid.ncv)
    UU_coeffs = ScalarCoeffs(grid.ncv)

    # Initial conditions
    U0 = 2
    P0 = 0

    # Initialize field variable arrays
    U = U0*np.ones(grid.ncv+2)
    P = P0*np.ones(grid.ncv+2)

    # Initialize advecting velocity and damping coefficient array
    dhat = np.zeros(grid.ncv+1)
    Uhe = U0*np.ones(grid.ncv+1)

    # Define boundary conditions for velocity
    U_west_bc = DirichletBc(U, grid, U0, BoundaryLocation.WEST)
    U_east_bc = NeumannBc(U, grid, 0, BoundaryLocation.EAST)

    # Define boundary conditions for pressure
    #   - Once ExtrapolatedBc is complete, change the boundary condition
    P_west_bc = ExtrapolatedBc(P, grid, BoundaryLocation.WEST)
    #P_west_bc = NeumannBc(P, grid, 0, BoundaryLocation.WEST)
    P_east_bc = DirichletBc(P, grid, 0, BoundaryLocation.EAST)

    # Apply boundary conditions
    U_west_bc.apply()
    U_east_bc.apply()
    P_west_bc.apply()
    P_east_bc.apply()

    # Create list to store the solutions at each iteration
    U_solns = [np.copy(U)]
    P_solns = [np.copy(P)]

    # Define the transient model
    Uold = np.copy(U)
    transient = FirstOrderTransientModel(grid, U, Uold, rho, 1, dt)

    # Define the diffusion model
    diffusion = DiffusionModel(grid, U, mu, U_west_bc, U_east_bc)

    # Define the advection model
    advection = QuickAdvectionModel(grid, U, Uhe, rho, 1, U_west_bc, U_east_bc)

    # Define the pressure force model
    pressure = PressureForceModel(grid, P, P_west_bc, P_east_bc)

    # Define advecting velocity model
    advecting = AdvectingVelocityModel(grid, dhat, Uhe, P, U, UU_coeffs)

    # Define conservation of mass equation
    mass = MassConservationEquation(grid, U, P, dhat, Uhe, rho, 
                                    P_west_bc, P_east_bc, U_west_bc, U_east_bc)

    # Loop through all timesteps
    for tStep in range(nTime):
        # Update the time information
        time += dt

        # Print the timestep information
        print("Timestep = {}; Time = {}".format(tStep, time))

        # Store the "old" velocity field
        Uold[:] = U[:]

        # Iterate until the solution is converged
        for i in range(maxIter):

            # Zero all of the equations
            PP_coeffs.zero()
            PU_coeffs.zero()
            UU_coeffs.zero()
            UP_coeffs.zero()     

            # Assemble the momentum equations
            #   Note: do this before mass, because the coeffs are needed to compute advecting velocity
            UU_coeffs = diffusion.add(UU_coeffs)
            UU_coeffs = advection.add(UU_coeffs)
            UU_coeffs = transient.add(UU_coeffs)
            UP_coeffs = pressure.add(UP_coeffs)

            # Assemble the mass equations
            advecting.update()
            PP_coeffs, PU_coeffs = mass.add(PP_coeffs, PU_coeffs)

            # Compute residuals and check for convergence
            PmaxResid = norm(PU_coeffs.rP + PP_coeffs.rP, np.inf)
            PavgResid = np.mean(np.absolute(PU_coeffs.rP + PP_coeffs.rP))
            UmaxResid = norm(UU_coeffs.rP + UP_coeffs.rP, np.inf)
            UavgResid = np.mean(np.absolute(UU_coeffs.rP + UP_coeffs.rP))
            print("Iteration = {}.".format(i))
            print("  Mass:     Max. Resid. = {}; Avg. Resid. = {}".format(PmaxResid, PavgResid))
            print("  Momentum: Max. Resid. = {}; Avg. Resid. = {}".format(UmaxResid, UavgResid))
            if PmaxResid < converged and UmaxResid < converged:
                break

            # Solve the sparse matrix system
            dP, dU = solve(PP_coeffs, PU_coeffs, UP_coeffs, UU_coeffs)

            # Update the solutions 
            P[1:-1] += dP
            U[1:-1] += dU

            # Update boundary conditions
            U_west_bc.apply()
            U_east_bc.apply()
            P_west_bc.apply()
            P_east_bc.apply()

            # Update the advecting velocities
            advecting.update()

            # Store the solution
            P_solns.append(np.copy(P))
            U_solns.append(np.copy(U))
        
    return P_solns

